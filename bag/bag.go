package bag

type Node struct {
	Item interface{}
	Next *Node
}

type Bag struct {
	First *Node
	N     int
}

func NewBag() Bag {
	return Bag{}
}

func (bag *Bag) IsEmpty() bool {
	return bag.First == nil
}

func (bag *Bag) Size() int {
	return bag.N
}

func (bag *Bag) Add(item interface{}) {
	oldfirst := bag.First
	bag.First = &Node{item, oldfirst}
	bag.N++
}

func (bag *Bag) Each(f func(*Node)) {
	node := bag.First
	for node != nil {
		f(node)
		node = node.Next
	}
}
