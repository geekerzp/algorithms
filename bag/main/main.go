package main

import (
	"algorithms/bag"
	"fmt"
)

func main() {
	b := bag.NewBag()
	b.Add("hello")
	b.Add("world")
	fmt.Printf("size of bag = %d\n", b.Size())
	b.Each(func(node *bag.Node) {
		println(node.Item.(string))
	})
}
