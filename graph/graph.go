package graph

import "algorithms/bag"

type Graph struct {
	V   int
	E   int
	Adj []*bag.Bag
}

func NewGraph(V int) *Graph {
	graph := new(Graph)
	graph.V = V
	graph.Adj = make([]*bag.Bag, V)
	for v := 0; v < V; v++ {
		graph.Adj[v] = new(bag.Bag)
	}
	return graph
}

func (graph *Graph) AddEdge(v, w int) {
	graph.E++
	graph.Adj[v].Add(w)
	graph.Adj[w].Add(v)
}

func (graph *Graph) AdjEach(v int, f func(w int)) {
	graph.Adj[v].Each(func(node *bag.Node) {
		f(node.Item.(int))
	})
}
