package main

import (
	"algorithms/digraph"
	"algorithms/topological"
)

func main() {
	dg := digraph.NewDigraph(13)
	dg.AddEdge(0, 1)
	dg.AddEdge(0, 5)
	dg.AddEdge(0, 6)
	dg.AddEdge(2, 0)
	dg.AddEdge(2, 3)
	dg.AddEdge(3, 5)
	dg.AddEdge(5, 4)
	dg.AddEdge(6, 4)
	dg.AddEdge(6, 9)
	dg.AddEdge(7, 6)
	dg.AddEdge(8, 7)
	dg.AddEdge(9, 10)
	dg.AddEdge(9, 11)
	dg.AddEdge(9, 12)
	dg.AddEdge(11, 12)

	t := topological.NewTopological(dg)
	t.OrderEach(func(v int) {
		print(v, " ")
	})
}
