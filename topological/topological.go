package topological

import (
	"algorithms/depth_first_order"
	"algorithms/digraph"
	"algorithms/directed_cycle"
)

type Topological struct {
	Order []int
}

func NewTopological(G *digraph.Digraph) *Topological {
	topological := new(Topological)
	cyclefinder := directed_cycle.NewDirectedCycle(G)
	if !cyclefinder.HasCycle() {
		dfs := depth_first_order.NewDepthFirstOrder(G)
		topological.Order = dfs.ReversePost
	}

	return topological
}

func (t *Topological) IsDAG() bool {
	return t.Order != nil
}

func (t *Topological) OrderEach(f func(v int)) {
	for _, v := range t.Order {
		f(v)
	}
}
