package main

import (
	"algorithms/digraph"
	"algorithms/directed_cycle"
	"github.com/davecgh/go-spew/spew"
)

func main() {
	dg := digraph.NewDigraph(13)
	dg.AddEdge(0, 1)
	dg.AddEdge(0, 5)
	dg.AddEdge(2, 0)
	dg.AddEdge(2, 3)
	dg.AddEdge(3, 2)
	dg.AddEdge(3, 5)
	dg.AddEdge(4, 2)
	dg.AddEdge(4, 3)
	dg.AddEdge(5, 4)
	dg.AddEdge(6, 0)
	dg.AddEdge(6, 4)
	dg.AddEdge(6, 8)
	dg.AddEdge(6, 9)
	dg.AddEdge(7, 6)
	dg.AddEdge(7, 9)
	dg.AddEdge(8, 6)
	dg.AddEdge(9, 10)
	dg.AddEdge(9, 11)
	dg.AddEdge(10, 12)
	dg.AddEdge(11, 4)
	dg.AddEdge(11, 12)
	dg.AddEdge(12, 9)

	dc := directed_cycle.NewDirectedCycle(dg)
	spew.Dump(dc.HasCycle())
	spew.Dump(dc.Cycle)
}
