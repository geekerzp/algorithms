package directed_cycle

import "algorithms/digraph"

type DirectedCycle struct {
	Marked  []bool
	EdgeTo  []int
	Cycle   []int
	OnStack []bool
}

func NewDirectedCycle(G *digraph.Digraph) *DirectedCycle {
	directedCycle := new(DirectedCycle)
	directedCycle.OnStack = make([]bool, G.V)
	directedCycle.EdgeTo = make([]int, G.V)
	directedCycle.Marked = make([]bool, G.V)
	for v := 0; v < G.V; v++ {
		if !directedCycle.Marked[v] {
			directedCycle.Dfs(G, v)
		}
	}
	return directedCycle
}

func (directedCycle *DirectedCycle) Dfs(G *digraph.Digraph, v int) {
	directedCycle.OnStack[v] = true
	directedCycle.Marked[v] = true
	G.AdjEach(v, func(w int) {
		if directedCycle.HasCycle() {
			return
		} else if !directedCycle.Marked[w] {
			directedCycle.EdgeTo[w] = v
			directedCycle.Dfs(G, w)
		} else if directedCycle.OnStack[w] {
			for x := v; x != w; x = directedCycle.EdgeTo[x] {
				directedCycle.Cycle = append([]int{x}, directedCycle.Cycle...)
			}
			directedCycle.Cycle = append([]int{w}, directedCycle.Cycle...)
			directedCycle.Cycle = append([]int{v}, directedCycle.Cycle...)
		}
	})
	directedCycle.OnStack[v] = false
}

func (directedCycle *DirectedCycle) HasCycle() bool {
	return directedCycle.Cycle != nil
}
