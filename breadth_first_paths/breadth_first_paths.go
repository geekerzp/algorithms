package breadth_first_paths

import "algorithms/graph"

type BreadthFirstPaths struct {
	Marked []bool
	EdgeTo []int
	S      int
}

func NewBreadthFirstPaths(G graph.Graph, s int) *BreadthFirstPaths {
	bfp := new(BreadthFirstPaths)
	bfp.Marked = make([]bool, G.V)
	bfp.EdgeTo = make([]int, G.V)
	bfp.S = s
	bfp.bfs(G, s)
	return bfp
}

func (bfp *BreadthFirstPaths) bfs(G graph.Graph, s int) {
	var queue []int
	bfp.Marked[s] = true
	queue = append(queue, s)
	for len(queue) != 0 {
		var v int
		v, queue = queue[0], queue[1:len(queue)]
		G.AdjEach(v, func(w int) {
			if !bfp.Marked[w] {
				bfp.EdgeTo[w] = v
				bfp.Marked[w] = true
				queue = append(queue, w)
			}
		})
	}
}

func (bfp *BreadthFirstPaths) HasPathTo(v int) bool {
	return bfp.Marked[v]
}

func (bfp *BreadthFirstPaths) PathTo(v int) []int {
	if !bfp.HasPathTo(v) {
		return nil
	}
	var path []int
	for x := v; x != bfp.S; x = bfp.EdgeTo[x] {
		path = append([]int{x}, path...)
	}
	path = append([]int{bfp.S}, path...)
	return path
}
