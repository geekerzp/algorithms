package digraph

import "algorithms/bag"

type Digraph struct {
	V   int
	E   int
	Adj []*bag.Bag
}

func NewDigraph(V int) *Digraph {
	digraph := new(Digraph)
	digraph.V = V
	digraph.E = 0
	digraph.Adj = make([]*bag.Bag, V)
	for v := 0; v < V; v++ {
		digraph.Adj[v] = new(bag.Bag)
	}
	return digraph
}

func (digraph *Digraph) AddEdge(v, w int) {
	digraph.Adj[v].Add(w)
	digraph.E++
}

func (digraph *Digraph) AdjEach(v int, f func(w int)) {
	digraph.Adj[v].Each(func(node *bag.Node) {
		f(node.Item.(int))
	})
}

func (digraph *Digraph) Reverse() *Digraph {
	R := NewDigraph(digraph.V)
	for v := 0; v < digraph.V; v++ {
		digraph.AdjEach(v, func(w int) {
			R.AddEdge(w, v)
		})
	}
	return R
}
