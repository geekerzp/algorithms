package depth_first_order

import (
	"algorithms/digraph"
)

type DepthFirstOrder struct {
	Marked      []bool
	Pre         []int
	Post        []int
	ReversePost []int
}

func NewDepthFirstOrder(G *digraph.Digraph) *DepthFirstOrder {
	dfo := new(DepthFirstOrder)
	dfo.Marked = make([]bool, G.V)

	for v := 0; v < G.V; v++ {
		if !dfo.Marked[v] {
			dfo.Dfs(G, v)
		}
	}

	return dfo
}

func (dfo *DepthFirstOrder) Dfs(G *digraph.Digraph, v int) {
	dfo.Pre = append(dfo.Pre, v)

	dfo.Marked[v] = true
	G.AdjEach(v, func(w int) {
		if !dfo.Marked[w] {
			dfo.Dfs(G, w)
		}
	})

	dfo.Post = append(dfo.Post, v)
	dfo.ReversePost = append([]int{v}, dfo.ReversePost...)
}

func (dfo *DepthFirstOrder) PreEach(f func(v int)) {
	for _, v := range dfo.Pre {
		f(v)
	}
}

func (dfo *DepthFirstOrder) PostEach(f func(v int)) {
	for _, v := range dfo.Post {
		f(v)
	}
}

func (dfo *DepthFirstOrder) ReversePostEach(f func(v int)) {
	for _, v := range dfo.ReversePost {
		f(v)
	}
}
