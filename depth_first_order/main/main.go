package main

import (
	"algorithms/depth_first_order"
	"algorithms/digraph"
)

func main() {
	dg := digraph.NewDigraph(13)
	dg.AddEdge(0, 1)
	dg.AddEdge(0, 5)
	dg.AddEdge(0, 6)
	dg.AddEdge(2, 0)
	dg.AddEdge(2, 3)
	dg.AddEdge(3, 5)
	dg.AddEdge(5, 4)
	dg.AddEdge(6, 4)
	dg.AddEdge(6, 9)
	dg.AddEdge(7, 6)
	dg.AddEdge(8, 7)
	dg.AddEdge(9, 10)
	dg.AddEdge(9, 11)
	dg.AddEdge(9, 12)
	dg.AddEdge(11, 12)

	dfo := depth_first_order.NewDepthFirstOrder(dg)

	print("Preorder: ")
	dfo.PreEach(func(v int) {
		print(v, " ")
	})
	println()

	print("Postorder: ")
	dfo.PostEach(func(v int) {
		print(v, " ")
	})
	println()

	print("Reverseorder: ")
	dfo.ReversePostEach(func(v int) {
		print(v, " ")
	})
	println()
}
