package bst

type Comparable interface {
	Compare(Comparable) int
}

type IntKey int

func (key IntKey) Compare(other Comparable) int {
	if key < other.(IntKey) {
		return -1
	} else if key > other.(IntKey) {
		return 1
	} else {
		return 0
	}
}

type Node struct {
	Key   Comparable
	Value interface{}
	Left  *Node
	Right *Node
	N     int
}

type BST struct {
	root *Node
}

func (bst BST) Size() int {
	return bst.SizeOfNode(bst.root)
}

func (bst BST) SizeOfNode(x *Node) int {
	if x == nil {
		return 0
	} else {
		return x.N
	}
}

func (bst BST) Get(key Comparable) interface{} {
	return bst.GetOnNode(bst.root, key)
}

func (bst BST) GetOnNode(x *Node, key Comparable) interface{} {
	if x == nil {
		return nil
	}
	cmp := key.Compare(x.Key)
	if cmp < 0 {
		return bst.GetOnNode(x.Left, key)
	} else if cmp > 0 {
		return bst.GetOnNode(x.Right, key)
	} else {
		return x.Value
	}
}

func (bst *BST) Put(key Comparable, val interface{}) {
	bst.root = bst.PutOnNode(bst.root, key, val)
}

func (bst *BST) PutOnNode(x *Node, key Comparable, val interface{}) *Node {
	if x == nil {
		return &Node{Key: key, Value: val, N: 1}
	}
	cmp := key.Compare(x.Key)
	if cmp < 0 {
		x.Left = bst.PutOnNode(x.Left, key, val)
	} else if cmp > 0 {
		x.Right = bst.PutOnNode(x.Right, key, val)
	} else {
		x.Value = val
	}
	x.N = bst.SizeOfNode(x.Left) + bst.SizeOfNode(x.Right) + 1
	return x
}
